from django.contrib import admin
from . import models

class HeroAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias')

class HotelAdmin(admin.ModelAdmin):
    list_display = ('hotel_name', 'address')

class PriceAdmin(admin.ModelAdmin):
    list_display = ('date', 'room_type', 'price')

class RoomAdmin(admin.ModelAdmin):
    list_display = ('hotel', 'room_type', 'room_number', 'room_status')

class ReservationAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'customer_name', 'booked_room_count', 'checkin_date', 'checkout_date', 'hotel')

class StayAdmin(admin.ModelAdmin):
    list_display = ('reservation', 'guest_name', 'room')

class StayRoomAdmin(admin.ModelAdmin):
    list_display = ('stay', 'room', 'date')

class PromoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in models.Promo._meta.fields if field.name != "id"]

# admin.site.register(models.Hero, HeroAdmin)
admin.site.register(models.Hotel, HotelAdmin)
admin.site.register(models.RoomType)
admin.site.register(models.Price, PriceAdmin)
admin.site.register(models.Room, RoomAdmin)
admin.site.register(models.Reservation, ReservationAdmin)
admin.site.register(models.Stay, StayAdmin)
admin.site.register(models.StayRoom, StayRoomAdmin)
admin.site.register(models.Promo, PromoAdmin)

