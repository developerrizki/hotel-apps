from os import name
from django.test import TestCase
from .models import Hero

class HeroTestCase(TestCase):
    def setUp(self):
        Hero.objects.create(name="Iron Man", alias="Tony Stark")

    def test_get_spesific_heroes(self):
        ironman = Hero.objects.get(name="Iron Man")
        self.assertEqual(ironman.name, "Iron Man")
