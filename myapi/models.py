from django.db import models

class Hero(models.Model):
    name = models.CharField(max_length=100)
    alias = models.CharField(max_length=60)

    def __str__(self):
        return self.name

class Hotel(models.Model):
    hotel_name = models.CharField(max_length=256)
    address = models.TextField(null="true")

    def __str__(self):
        return self.hotel_name

class RoomType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Price(models.Model):
    date = models.DateField()
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)
    price = models.BigIntegerField(default=0)

    def __str__(self):
        return str(self.room_type)

class Room(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    room_type = models.ForeignKey(RoomType, on_delete=models.CASCADE)
    room_number = models.CharField(max_length=50)
    room_status = models.CharField(max_length=50)

    def __str__(self):
        return self.room_number

class Reservation(models.Model):
    order_id = models.CharField(max_length=100)
    customer_name = models.CharField(max_length=150)
    booked_room_count = models.IntegerField(default=0)
    checkin_date = models.DateField()
    checkout_date = models.DateField()
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)

    def __str__(self):
        return self.order_id

class Stay(models.Model):
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE)
    guest_name = models.CharField(max_length=150)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return self.guest_name

class StayRoom(models.Model):
    stay = models.ForeignKey(Stay, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    date = models.DateField()

    def __str__(self):
        return str(self.date)

class Promo(models.Model):
    code = models.CharField(max_length=50)
    type = models.CharField(max_length=100)
    amount = models.IntegerField(default=0)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    minimum_room = models.IntegerField(default=0)
    minimum_nights = models.IntegerField(default=0)
    checkin_day = models.IntegerField(default=0)
    booking_day = models.IntegerField(default=0)
    start_hour = models.TimeField()
    end_hour = models.TimeField()
