from datetime import datetime
from rest_framework import serializers, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from . import models, serializers
from django.db.models import Min

class HotelViewSet(viewsets.ModelViewSet):
    queryset = models.Hotel.objects.all().order_by('id')
    serializer_class = serializers.HotelSerializer

class HotelView(APIView):
    def get(self, request):
        checkin_date = request.GET['checkin_date']
        checkout_date = request.GET['checkout_date']
        room_type_id = int(request.GET['room_type_id'])
        room_qty = int(request.GET['room_qty'])

        # Get Room filter with room type
        rooms = models.Room.objects.filter(
            room_status="available", room_type_id=room_type_id).all()

        room_array = []

        # Custom Response Room
        for room in rooms:
            # Get Price filter with date range and room type
            prices = models.Price.objects.filter(date__range=[checkin_date, checkout_date], room_type_id=room.room_type_id).all()
            price_array = []

            # Custom Response Price
            for price in prices:
                price_json = {
                    'date': price.date,
                    'price': price.price
                }
                
                # Add json to array
                price_array.append(price_json)

            room_json = {
                'room_id': room.id,
                'room_number': room.room_number,
                'price': price_array
            }

            # Add json to array
            room_array.append(room_json)

        # Get total price
        price_avg = models.Price.objects.filter(
            date__range=[checkin_date, checkout_date], room_type_id=room_type_id).all()

        price_avg = price_avg.aggregate(Min('price'))

        # Set response
        response = {
            'room_qty': room_qty,
            'room_type_id': room_type_id,
            'checkin_date': checkin_date,
            'checkout_date': checkout_date,
            'total_price': price_avg['price__min'],
            'available_room': room_array
        }

        return Response(response, 200)

class PromoView(APIView):
    def get(self, request):
        checkin_date = request.GET['checkin_date']
        checkout_date = request.GET['checkout_date']
        room_type_id = int(request.GET['room_type_id'])
        room_qty = int(request.GET['room_qty'])

        # Calculate nights from checkin and checkout date
        night = self.get_night_count(request)

        # Get Room filter with room type
        rooms = models.Room.objects.filter(
            room_status="available", room_type_id=room_type_id).all()

        room_array = []

        # Custom Response Room
        for room in rooms:
            # Get Price filter with date range and room type
            prices = models.Price.objects.filter(
                date__range=[checkin_date, checkout_date], room_type_id=room.room_type_id).all()
            price_array = []

            # Custom Response Price
            for price in prices:

                # Get Promo
                price_promo = 0
                promo = models.Promo.objects.filter(
                    room_id=room.id, minimum_nights__gte=night, minimum_room__gte=room_qty,
                    start_hour__lte=datetime.now().strftime('%H:%M:%S'), end_hour__gte=datetime.now().strftime('%H:%M:%S')
                )

                if (promo):
                    price_promo = self.get_calculate_promo(promo, price.price)

                final_price = price.price - price_promo

                price_json = {
                    'date': price.date,
                    'price': price.price,
                    'promo': price_promo,
                    'final_price': final_price
                }

                # Add json to array
                price_array.append(price_json)

            room_json = {
                'room_id': room.id,
                'room_number': room.room_number,
                'price': price_array
            }

            # Add json to array
            room_array.append(room_json)

        # Get total price
        price_avg = models.Price.objects.filter(
            date__range=[checkin_date, checkout_date], room_type_id=room_type_id).all()

        price_avg = price_avg.aggregate(Min('price'))

        # Set response
        response = {
            'room_qty': room_qty,
            'room_type_id': room_type_id,
            'checkin_date': checkin_date,
            'checkout_date': checkout_date,
            'total_price': price_avg['price__min'],
            'available_room': room_array
        }

        return Response(response, 200)

    def get_night_count(self, request):
        start_date = datetime.strptime(
            request.GET['checkin_date'], "%Y-%m-%d")
        end_date = datetime.strptime(
            request.GET['checkout_date'], "%Y-%m-%d")

        diff = abs((end_date-start_date).days)
        
        return diff - 1

    def get_calculate_promo(self, promo, price):
        price_promo = 0

        if (promo.type == "percentage") :
            price_promo = (promo.amount/100) * price
        else :
            price_promo = promo.amount

        return price_promo



