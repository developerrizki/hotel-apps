from operator import mod
from django.db.models import fields
from rest_framework import serializers
from . import models

class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Hero
        fields = ('id', 'name', 'alias')

class HotelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Hotel
        fields = ('id', 'hotel_name', 'address')
