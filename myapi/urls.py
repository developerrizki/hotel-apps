from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'hotels', views.HotelViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('search-hotel', views.HotelView.as_view()),
    path('search-promo-hotel', views.PromoView.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]