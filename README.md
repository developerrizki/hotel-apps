# Prepare Environment
1. Download & Install python
    - https://www.python.org/downloads/
2. Install pip
    - Mac OS : https://www.geeksforgeeks.org/how-to-install-pip-in-macos/
    - Windows : https://www.geeksforgeeks.org/how-to-install-pip-on-windows/
3. Install pipenv
    - python2 : pip install pipenv
    - python3 : pip3 install pipenv

# Recommended IDE
1. Install VSCode : https://code.visualstudio.com/download
2. Install VSCode Extenstion : Python (https://marketplace.visualstudio.com/items?itemName=ms-python.python)

# Prepare Start Project
1. mkdir hotel-apps
2. cd hotel-apps
3. Install Django run this script inside hotel-apps folder
    - pipenv install django
4. Install Django Rest Framework run this script inside hotel-apps folder
    - pipenv install djangoresframework
5. Copy path of venv after install *pipenv install django* or you can run again *pipenv shell*
6. Open folder hotel-apps on VSCode
7. Click View > Command Palette > Search *Python: Select Interpreter*
8. Copy path venv to this, and enter
9. Open Terminal on VSCode. (Click View > Terminal)
10. Run makemigrations : 
    - python2 : python manage.py makemigrations
    - python3 : python3 manage.py makemigrations 
11. Run migrate : 
    - python2 : python manage.py migrate
    - python3 : python3 manage.py migrate 
12. Run local server (localhost:8000) :
    - python2 : python manage.py runserver
    - python3 : python3 manage.py runserver 

# Heroku
- url : https://hotel-apps-python.herokuapp.com/
please check with this url on postman before you test, if not run .. you can try at local

# Postman
- https://documenter.getpostman.com/view/226082/UVXkoaws

# Test Search Hotel
- Data for search, available start form 2022-01-01 until 2022-01-05

# Admin Panel
- url : https://hotel-apps-python.herokuapp.com/admin
- account : {"username":"devadm", "password":"kamuakukita"}
